import React from "react";
import ReactDOM from "react-dom/client";

import TableBody from "./Elements/Element_tableBody";

import "./style.css";

// Основна функція створює таблицю знаків зодіака
function Products() {
  return (
    <>
      <div className="wrapper">
        <h1>Знаки зодіаку</h1>
        <TableBody />
      </div>
    </>
  );
}

// Рендер всієї таблиці на HTML сторінку
const root = ReactDOM.createRoot(document.querySelector(".one"));
root.render(<Products />);
