// Масив знаків зодіака
const horoscope = [
  {
    name: "Овен",
    period_sign: "21 березня – 19 квітня",
  },
  {
    name: "Телець",
    period_sign: "20 квітня – 20 травня",
  },
  {
    name: "Близнюки",
    period_sign: "21 травня – 21 червня",
  },
  {
    name: "Рак",
    period_sign: "22 червня – 22 липня",
  },
  {
    name: "Лев",
    period_sign: "23 липня – 22 серпня",
  },
  {
    name: "Діва",
    period_sign: "23 серпня – 22 вересня",
  },
  {
    name: "Терези",
    period_sign: "23 вересня – 22 жовтня",
  },
  {
    name: "Скорпіон",
    period_sign: "23 жовтня – 21 листопада",
  },
  {
    name: "Стрілець",
    period_sign: "22 листопада – 21 грудня",
  },
  {
    name: "Козоріг",
    period_sign: "22 грудня – 19 січня",
  },
  {
    name: "Водолій",
    period_sign: "20 січня – 18 лютого",
  },
  {
    name: "Риби",
    period_sign: "19 лютого – 20 березня",
  },
];

export default horoscope;
