// Імпорт елемента рядка таблиці
import TrTable from "../Element_tr";

// Елемент таблиці знаків зодіака
const TableBody = function () {
  return (
    <table>
      <thead>
        <tr>
          <th>Назва</th>
          <th> Період дії знаку</th>
        </tr>
      </thead>
      <tbody>
        <TrTable />
      </tbody>
    </table>
  );
};

export default TableBody;
