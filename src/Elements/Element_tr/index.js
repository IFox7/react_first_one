// Імпорт масиву гороскоп
import horoscope from "../../horoscop_data";

// Елемент рядка таблиці. Ячейки створюються перебором масива гороскоп методом 'map'
const Tr_table = function () {
  return horoscope.map((item, index) => {
    return (
      <tr key={index} className="table_rows">
        <td>{item.name}</td>
        <td>{item.period_sign}</td>
      </tr>
    );
  });
};

// Експортуємо елемент рядка таблиці для подальшого використання
export default Tr_table;
